OPERATING SYSTEM (CIT3002)
Simulation Date       : 21/06/2019
Simulation Start Time : 00:40 hr

		Integer Elements :
				Key:10	Value:223
				Key:9	Value:622
				Key:8	Value:407
				Key:7	Value:822
				Key:6	Value:158
				Key:5	Value:519
				Key:4	Value:735
				Key:3	Value:776
				Key:2	Value:301
				Key:1	Value:198
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PID		TASK		KEY      VALUE	     ATTEMPTS    START TIME	END TIME    SLEEP TIME	   Algorithm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
199	Sorted The Records 				1	    1	       	4		 0	   FCFS
				Key:6	Value:158
				Key:1	Value:198
				Key:10	Value:223
				Key:2	Value:301
				Key:8	Value:407
				Key:5	Value:519
				Key:9	Value:622
				Key:4	Value:735
				Key:3	Value:776
				Key:7	Value:822
5	Removed a Record   -	6        158		1	    5	       	6		 0	   FCFS
580	Removed a Record   -	1        198		1	    7	       	9		 2	   FCFS
17	Sorted The Records 				1	    10	       	11		 3	   FCFS
				Key:10	Value:223
				Key:2	Value:301
				Key:8	Value:407
				Key:5	Value:519
				Key:9	Value:622
				Key:4	Value:735
				Key:3	Value:776
				Key:7	Value:822
421	Calculated the Total	Sum:     4405		1	    11	       	11		 3	   RR
346	Sorted The Records 				1	    12	       	15		 5	   FCFS
				Key:10	Value:223
				Key:2	Value:301
				Key:8	Value:407
				Key:5	Value:519
				Key:9	Value:622
				Key:4	Value:735
				Key:3	Value:776
				Key:7	Value:822
544	Removed a Record   -	10        223		1	    16	       	17		 1	   FCFS
188	Added a New Record +	3        707		1	    18	       	19		 2	   FCFS
172	Removed a Record   -	3        707		1	    20	       	21		 0	   FCFS
270	Removed a Record   -	2        301		1	    22	       	26		 3	   FCFS
150	Retrieved a Record	5        519		2	    10	       	28		 4	   RR
429	Removed a Record   -	8        407		1	    27	       	29		 0	   FCFS
