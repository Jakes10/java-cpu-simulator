OPERATING SYSTEM (CIT3002)
Simulation Date       : 02/04/2019
Simulation Start Time : 21:42 hr

		Integer Elements :
				Key:10	Value:832
				Key:9	Value:217
				Key:8	Value:387
				Key:7	Value:617
				Key:6	Value:282
				Key:5	Value:277
				Key:4	Value:604
				Key:3	Value:406
				Key:2	Value:477
				Key:1	Value:374
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PID		TASK		KEY      VALUE	     ATTEMPTS    START TIME	END TIME    SLEEP TIME	   Algorithm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
368	Calculated the Total	Sum:     4473		1	    0	       	2		 0	   RR
105	Retrieved a Record	6        282		1	    1	       	2		 1	   RR
299	Removed a Record   -	10        832		1	    1	       	3		 2	   FCFS
227	Calculated the Total	Sum:     3641		1	    2	       	4		 0	   FCFS
155	Added a New Record +	10        717		1	    5	       	8		 1	   FCFS
562	Retrieved a Record	4        604		1	    7	       	9		 0	   RR
538	Retrieved a Record	1        374		1	    7	       	9		 1	   RR
456	Calculated the Total	Sum:     4358		1	    5	       	10		 0	   FCFS
81	Removed a Record   -	10        717		1	    11	       	15		 0	   FCFS
487	Removed a Record   -	9        217		1	    16	       	19		 0	   FCFS
