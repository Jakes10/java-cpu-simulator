OPERATING SYSTEM (CIT3002)
Simulation Date       : 11/05/2019
Simulation Start Time : 19:30 hr

		Integer Elements :
				Key:10	Value:598
				Key:9	Value:861
				Key:8	Value:720
				Key:7	Value:492
				Key:6	Value:285
				Key:5	Value:423
				Key:4	Value:416
				Key:3	Value:268
				Key:2	Value:316
				Key:1	Value:188
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PID		TASK		KEY      VALUE	     ATTEMPTS    START TIME	END TIME    SLEEP TIME	   Algorithm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
549	Removed a Record   -	10        598		1	    1	       	3		 3	   FCFS
278	Added a New Record +	10        281		1	    4	       	7		 0	   FCFS
217	Sorted The Records 				1	    8	       	9		 0	   FCFS
				Key:1	Value:188
				Key:3	Value:268
				Key:10	Value:281
				Key:6	Value:285
				Key:2	Value:316
				Key:4	Value:416
				Key:5	Value:423
				Key:7	Value:492
				Key:8	Value:720
				Key:9	Value:861
318	Retrieved a Record	3        268		1	    10	       	13		 0	   RR
567	Sorted The Records 				1	    10	       	14		 1	   FCFS
				Key:1	Value:188
				Key:3	Value:268
				Key:10	Value:281
				Key:6	Value:285
				Key:2	Value:316
				Key:4	Value:416
				Key:5	Value:423
				Key:7	Value:492
				Key:8	Value:720
				Key:9	Value:861
83	Retrieved a Record	7        492		1	    15	       	17		 2	   RR
297	Sorted The Records 				1	    16	       	17		 3	   RR
				Key:1	Value:188
				Key:3	Value:268
				Key:10	Value:281
				Key:6	Value:285
				Key:2	Value:316
				Key:4	Value:416
				Key:5	Value:423
				Key:7	Value:492
				Key:8	Value:720
				Key:9	Value:861
588	Sorted The Records 				1	    16	       	18		 5	   RR
				Key:1	Value:188
				Key:3	Value:268
				Key:10	Value:281
				Key:6	Value:285
				Key:2	Value:316
				Key:4	Value:416
				Key:5	Value:423
				Key:7	Value:492
				Key:8	Value:720
				Key:9	Value:861
419	Removed a Record   -	1        188		1	    15	       	19		 0	   FCFS
578	Removed a Record   -	3        268		2	    11	       	21		 3	   RR
236	Removed a Record   -	10        281		2	    11	       	21		 1	   RR
141	Removed a Record   -	6        285		1	    20	       	22		 0	   FCFS
192	Calculated the Total	Sum:     3228		2	    21	       	23		 1	   FCFS
