OPERATING SYSTEM (CIT3002)
Simulation Date       : 02/04/2019
Simulation Start Time : 21:41 hr

		Integer Elements :
				Key:10	Value:354
				Key:9	Value:396
				Key:8	Value:857
				Key:7	Value:636
				Key:6	Value:663
				Key:5	Value:203
				Key:4	Value:487
				Key:3	Value:736
				Key:2	Value:737
				Key:1	Value:778
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
PID		TASK		KEY      VALUE	     ATTEMPTS    START TIME	END TIME    SLEEP TIME	   Algorithm
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
180	Removed a Record   -	10        354		1	    1	       	1		 0	   FCFS
69	Calculated the Total	Sum:     5493		1	    1	       	3		 3	   RR
549	Retrieved a Record	1        778		1	    2	       	3		 0	   FCFS
574	Removed a Record   -	9        396		1	    2	       	6		 0	   FCFS
9	Removed a Record   -	8        857		1	    7	       	11		 0	   FCFS
86	Added a New Record +	8        345		1	    12	       	16		 1	   FCFS
456	Removed a Record   -	8        345		1	    17	       	21		 0	   FCFS
66	Removed a Record   -	7        636		1	    22	       	23		 0	   FCFS
290	Retrieved a Record	4        487		1	    23	       	25		 5	   RR
257	Retrieved a Record	4        487		1	    23	       	25		 3	   RR
319	Retrieved a Record	2        737		1	    24	       	25		 2	   FCFS
352	Calculated the Total	Sum:     3604		1	    24	       	28		 0	   FCFS
364	Removed a Record   -	6        663		1	    29	       	32		 0	   FCFS
377	Removed a Record   -	5        203		2	    1	       	34		 0	   FCFS
260	Sorted The Records 				2	    2	       	36		 1	   FCFS
				Key:4	Value:487
				Key:3	Value:736
				Key:2	Value:737
				Key:1	Value:778
559	Sorted The Records 				2	    23	       	37		 0	   FCFS
				Key:4	Value:487
				Key:3	Value:736
				Key:2	Value:737
				Key:1	Value:778
